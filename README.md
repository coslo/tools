Modern tools for computational science
======================================

This repository hosts the material for the course "Modern tools for computational physics" for the [Ph.D. program](https://web.units.it/dottorato/fisica/) in Physics at the University of Trieste.

The material is available as

- [web page](https://coslo.frama.io/tools/index.html)
- org-mode files: [text](<https://framagit.org/coslo/tools/-/blob/master/docs/text.org), [code](https://framagit.org/coslo/tools/-/blob/master/docs/code.org), [project](https://framagit.org/coslo/tools/-/blob/master/docs/project.org)
- jupyter notebooks: [text](<https://framagit.org/coslo/tools/-/blob/master/docs/text.ipynb), [code](https://framagit.org/coslo/tools/-/blob/master/docs/code.ipynb), [project](https://framagit.org/coslo/tools/-/blob/master/docs/project.ipynb)

**Templates**:

- [Sphinx-based web page](https://framagit.org/coslo/template-docs)
- [Python package](https://framagit.org/coslo/template-python)
- [Bare-bones computational project](https://framagit.org/coslo/template-project)

**Author**

[Daniele Coslovich](https://www.units.it/daniele.coslovich/)
