.PHONY: all rst docs

all: docs

# Create rst file from source
rst:
	rm -f docs/*-[A-Z].rst
	../../usr/orgorg.py docs/text.org
	../../usr/orgorg.py docs/code.org
	../../usr/orgorg.py docs/project.org
	# echo "Coming soon..." > docs/code-A.rst
	# echo "--------------" >> docs/code-A.rst
	#echo "Coming soon..." > docs/project-A.rst
	#echo "--------------" >> docs/project-A.rst
	for file in docs/*-*.org; do emacs --batch -l ~/.emacs.d/init.el -l ~/.emacs.d/config.el --file=$$file -f org-rst-export-to-rst --kill; done
	for file in docs/*-*.rst; do sed -i 's/\\\*/\*/g' $$file; done
	rm -f docs/*-*.org

ipynb:
	orgnb docs/*.org

docs: rst ipynb
	make -C docs/ html
