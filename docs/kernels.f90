module kernels
  implicit none
contains
  subroutine daxpy(a, x, y, f)
    real(8), intent(in) :: a, x(:), y(:)
    real(8), intent(inout) :: f(:)
    f = x + a*y
  end subroutine daxpy

  subroutine dot(x, y, f)
    real(8), intent(in) :: x(:), y(:)
    real(8), intent(out) :: f
    f = dot_product(x, y)
  end subroutine dot
end module kernels

