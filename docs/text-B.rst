


Version control with git
------------------------

Useful links
~~~~~~~~~~~~

Authoritative sources of information on git:

- The *official* git documentation: `https://git-scm.com/doc <https://git-scm.com/doc>`_

- Beginner tutorial by *gitlab*: `https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html <https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html>`_

- Beginner tutorial by *github*: `https://docs.github.com/en/github/getting-started-with-github/quickstart <https://docs.github.com/en/github/getting-started-with-github/quickstart>`_

- Questions on *stack overflow*: `https://stackoverflow.com/questions/tagged/git <https://stackoverflow.com/questions/tagged/git>`_

Git in a nutshell
~~~~~~~~~~~~~~~~~

.. image:: ./images/Git-logo.png

Git is a **version control system** (VCS)

- Keep **history** of modifications to files

- Single-user or team **software development**

- Created by Linus Torvalds in 2005 to support the linux kernel development

- As of today, the **most popular** VCS out there

It is a *command line tool*. Check if you have it:

.. code:: bash

    git --version

::

    git version 2.34.1


**Found it?** Cool, then read on! If not: `https://git-scm.com/downloads <https://git-scm.com/downloads>`_

.. tip::

    Try out tab completion on the command above (while typing ``--version``!)

What will *you* use it for, today?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Learn how to keep text documents under version control

- Fine tune git configuration

- Collaboratively edit a text document (ex. scientific article, documentation, ...)

.. image:: ./images/docs1.png

What do *I* use it for?
~~~~~~~~~~~~~~~~~~~~~~~

Over the past 10+ years:

- **code development**

- **configuration files** (my ``.bashrc``, ``.emacs``, etc.)

- **research papers** - with *geek* colleagues only :-(

- website, curriculum, tutorials, notebooks, ...

All this can be done

- as a **single user** or in **collaboration** with others

- both **offline** on a machine (ex. my laptop) or...

- ... over a **network** (I currently use `https://framagit.org/ <https://framagit.org/>`_ as main server)

Quick start
~~~~~~~~~~~

To keep track of the modification history of the files in a folder, turn that folder into a git **repository**.

Increments in the modification history appear as ``commits``: each commit stores a set of modifications to the project, like **adding**, **modifying** or **removing** files.

Create a new folder, say ``project/,`` cd into it and initialize the repository

.. code:: bash

    mkdir project
    cd project
    git init

::

    Initialized empty Git repository in /home/coslo/teaching/tools/docs/project/.git/


.. note::

    The history of your repository, along with its configuration, are stored in the ``.git/`` folder. If you delete that folder, the git repository is gone!

Add a README file in the ``project`` folder

.. code:: bash

    echo "Hello World!" > README.md
    git status

::


    On branch master

    No commits yet

    Untracked files:
    ..." to include in what will be committed)
    	README.md

    nothing added to commit but untracked files present (use "git add" to track)

Now add the file to the project and commit the change

.. code:: bash

    git add .  # the dot will add all new files in the current folder
    git commit -m "Initial commit"

::


    [master (root-commit) 87dfb76] Initial commit
     1 file changed, 1 insertion(+)
     create mode 100644 README.md

More on commits
~~~~~~~~~~~~~~~

As you can see, this is done in two steps:

- ``add``: include the new file in the list of modifications to store ("stage")

- ``commit``: store the staged modifications in the repository

The ``-m`` flag allows you to leave a "commit message" to describe the change.

Modify the file and check the difference

.. code:: bash

    echo "Hello World, again!" >> README.md
    git diff

::


    diff --git a/README.md b/README.md
    index 980a0d5..5eb476f 100644
    --- a/README.md
    +++ b/README.md
    @@ -1 +1,2 @@
     Hello World!
    +Hello World, again!


Commit the change

.. code:: bash

    git commit -am "Update README"

::

    [master c8c02ae] Update README
     1 file changed, 1 insertion(+)


If we modify some files, we can stage the change and commit in one shot with the ``-a`` (or ``-all``) flag.

Have a look at what we have done so far

.. code:: sh

    git log

::

    commit c8c02ae78ac2269ee3c477633bbdf809b4a3bc41
    Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
    Date:   Thu Apr 4 12:10:45 2024 +0200

        Update README

    commit 87dfb767a1e466a3d4c4432719ab70db9b5a10e1
    Author: Daniele Coslovich <92140-coslo@users.noreply.framagit.org>
    Date:   Thu Apr 4 12:10:36 2024 +0200

        Initial commit

Finally, removing a file is similar to adding it

.. code:: bash

    touch useless_file.txt
    git add useless_file.txt
    git commit -m "Add useless file"

    git rm useless_file.txt
    git commit -m "Remove useless file"

::


    coslo@Latitude-7280:~/teaching/tools/docs/project$ [master 137a2b0] Add useless file
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 useless_file.txt
    coslo@Latitude-7280:~/teaching/tools/docs/project$ rm 'useless_file.txt'
    [master db42cdc] Remove useless file
     1 file changed, 0 insertions(+), 0 deletions(-)
     delete mode 100644 useless_file.txt


Using ``git rm`` instead of ``rm`` is not strictly necessary in this case. You can simply remove the file and then commit the change like this

.. code:: bash

    touch useless_file.txt
    git add useless_file.txt
    git commit -m "Add useless file"
    rm useless_file.txt
    git commit -m "Remove useless file" useless_file.txt

You may want to remove the file from the git repository, but keep a copy of it in the project folder. This is useful when you bulk added files to the project, but then realize you do not need to keep all of them under version control.

.. code:: bash

    touch file
    git add file
    git commit -m "Add unnecessary file"
    git rm --cached file
    ls file  # your file is still there

::

    [master (root-commit) 29fdb64] Add unnecessary file
     1 file changed, 0 insertions(+), 0 deletions(-)
     create mode 100644 file
    rm 'file'
    file


**Bottom lines**: 

Useful commands to check the status of your repository are

- ``git status``

- ``git diff``

- ``git log``

Useful command to handle files

- ``git add``

- ``git commit``

- ``git rm``

Fine-tuning
~~~~~~~~~~~

Personal information
^^^^^^^^^^^^^^^^^^^^

On the first commit, git might have asked you to provide your name and e-mail. This is not strictly necessary but you can do that with these commands

.. code:: bash

    git config user.name <your_name>
    git config user.email <your_email>

Add the ``--global`` flag to apply these settings globally for all your repositories

.. warning::

    *Privacy notes*:

    - If you plan to share your project publicly, consider using a dummy (inexistent) email account

    - git commits may reveal a lot of information about a user's habits...

Existing files
^^^^^^^^^^^^^^

What if the folder *already* contains files and/or sub-folders and we want to some (but not all) of them to the repository?

**Option 1**: add all the files then remove the ones you do not need

.. code:: bash

    git add .
    git rm --cached <not_needed_file>
    git commit -m "Add files"

**Option 2**: add only selected files

.. code:: bash

    git add <needed_file_1> <needed_file_2>
    git commit -m "Add files"

You can use the star syntax (ex. ``*.tex``) to match multiple file

Ignoring files
^^^^^^^^^^^^^^

You may want to avoid storing certain files in your git repository, for instance

- temporary or backup files (``*.~``, ``*.bak``)

- compilation artifacts and executables (``*.o``, ``*.so``, ``*.x``)

Create a text file called ``.gitignore`` at the root of your repositoriy and fill it with patterns matching the files to ignore

.. code:: bash

    cat > .gitignore <<EOF
    *~
    *.o
    EOF

    git add .gitignore
    git commit -am "Add gitignore"

From now on, ``git status`` will ignore these files and they will not be considered for commit

FAQ
~~~

How do I go back in time?
^^^^^^^^^^^^^^^^^^^^^^^^^

First look for the *hash* of the commit you are interested in

.. code:: sh

    git log --pretty=oneline

::

    f23903dbaad95525b9f93a2e85bc2a0cbbe02563 Remove useless file
    a595e3317b224aac0ef1752e2775c4d526859ca6 Add useless file
    ddbfdfc0f64c5e8b8fa614d04b66b7345d989626 Update README
    215049f1d757adcdf7a05ad3e5c253d4eb3b75f4 Initial commit


Check out a copy of the project as it was in that commit

.. code:: bash

    git checkout a730cb4f

To get back to the current state of the project

.. code:: bash

    git checkout master

How can I undo the last commit?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This is one of the `most popular question <https://stackoverflow.com/questions/927358/how-do-i-undo-the-most-recent-local-commits-in-git>`_ about git on stackoverflow.

.. code:: bash

    git commit -m "Something terribly misguided"
    git reset HEAD~1

This will undo the commit but keep your files as they were one commit before the last one. Then edit the files as necessary and commit again. The ``HEAD`` syntax refers to the "tip" of your currently checked out branch, while ``HEAD~1`` (or ``HEAD~``) means one commit before that.

How do I restore a file as it was at the latest commit?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You have edited some files in your project but you want to go back as they were at the last commit. If you are not interesting in those changes

.. code:: bash

    git restore <file>

If you want to keep those changes for later, use ``git stash`` instead.

Can I reword a commit message?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Yes, there is an interactive rebase mode to edit commit messages (and more!).

.. code:: bash

    git commit -m "A typoed comit mesage"
    git commit -m "A correct Commit message"
    git rebase -i HEAD~2

This will open the editor defined by the ``EDITOR`` environment variable (ex. ``nano``) and let you "reword" the second-to-last commit message. Follow the instructions and the save the files once you are done.

.. warning::

    The ``rebase`` interactive command allows you to rewrite commit history. Do not use it *after* you have shared your commits with others.

Do I *really* need to write those commit messages?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Writing commit messages may seem a burden (and actually, you can leave them blank) but it *is* important.

.. warning::

    You may be able to write commit messages directly from your IDE and/or text editor.

.. image:: ./images/git_commit_2x.png

There are well-established guidelines to write **good** commit messages, so that they are informative and easy to read
`https://cbea.ms/posts/git-commit/ <https://cbea.ms/posts/git-commit/>`_

**Structure**

- *Separate subject from body with a blank line*

- Use the body to explain what and why vs. how

**Style**

- *Use the imperative mood in the subject line*

- Capitalize the subject line

- Do not end the subject line with a period

- Limit the subject line to 50 characters

- Wrap the body at 72 characters

.. hint::

    Write "Fix typo in tutorial", not "fixed typo in tutorial."

Branches
~~~~~~~~

Branches allow you to keep **multiple versions of your project** in parallel.

Repositories are created with a default branch, typically called **master** or **main**. That's the branch to which we committed the modifications above.

Typical use case: develop a new feature in a separate *feature branch*, then merge it in the master branch.

.. image:: images/branch1.png

**Glossary**:

- *Fast-forward*: no commits between the creation of the new branch and the merge

- *Conflict*: concurrent modification to the same part of a file

Create a new branch and check it out

.. code:: bash

    git checkout -b new_cool_feature
    # commit commit commit ...

::

    Switched to a new branch 'new_cool_feature'


Show available branches

.. code:: bash

    git branch -va

::

      master           f23903d Remove useless file
    * new_cool_feature f23903d Remove useless file


The prompt of your terminal should tell you which branch you are working on

::

    coslo@local:~/teaching/tools/text/(new_cool_feature)$

If not, you may want to follow these instructions `https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/ <https://blog.sellorm.com/2020/01/13/add-the-current-git-branch-to-your-bash-prompt/>`_

When the new feature is ready, get back to the master branch and merge the feature branch

.. code:: bash

    git checkout master
    git merge new_cool_feature

::

    Switched to branch 'master'
    Already up to date.


To show a graph of the branches and their respective commit history, use

.. code:: bash

    git log --graph --all --decorate --oneline

If **conflicts** occur, you will have to modify the files to remove them. ``git status`` will tell you how to proceed.

.. warning::

    Conflict handling can be nasty... but don't be afraid!

Exercise: single-user workflow
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Make sure ``git`` is installed on your machine (if not, follow `https://git-scm.com/downloads <https://git-scm.com/downloads>`_)

- Download and extract the template project ``template-docs`` into ``docs/``

.. code:: bash

    git clone https://framagit.org/coslo/template-docs.git docs

- Turn it into a new git repository: remove the ``.git/`` folder, add all the relevant files and commit your changes

- Edit ``README.md`` by adding your name to the "Authors" section and commit the change

::

    Authors
    -------
    ...

- Familiarize yourself to the commands seen above, including those related to branching if you are already confident with the git basics
