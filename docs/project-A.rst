


Computational projects
----------------------

This tutorial will give an overview on tools to manage **computational projects** with a **reproducible research** approach.

**Examples of "projects"**:

- lab session

- internship project

- master thesis

- scientific article

More specifically, a "computational project" comprises 3 related parts:

1) **software environment**

2) **workflow**

3) **data**

.. image:: images/project.png

It is a good idea to keep *everything* related to a given project in one single folder.

See `Good Enough Practices in Scientific Computing <https://arxiv.org/abs/1609.00037>`_ for a more extended discussion and for further recommendations on how to shape your computational projects.

**Which project have you been working on?**

.. table::

Reproducible research
~~~~~~~~~~~~~~~~~~~~~

Carring out **reproducible research** means putting a colleague in the conditions to re-obtain our results

- in actual experiments: within **statistical uncertainty**

- in computer experiments and numerical analysis: typically **statistical uncertainty**, but sometimes even within **numerical precision**

*References*:

- `https://rlhick.people.wm.edu/posts/reproducible-research.html <https://rlhick.people.wm.edu/posts/reproducible-research.html>`_

- `http://faculty.washington.edu/rjl/talks/csgf08/csgf08.pdf <http://faculty.washington.edu/rjl/talks/csgf08/csgf08.pdf>`_

**Reproducibility** is achieved by providing

- workflow documentation

- output results

- (optionally) software environment description

- (optionally) input data

.. image:: images/weak_repr.png

**"Full reproducibility"** (or "repricability") is achieved by providing the full project stack

- software environment

- workflow

- data

.. table::

    +----------+----------------------------------+-------------------------------------+
    | \        | **Reproducibility**              | **Full reproducibility**            |
    +==========+==================================+=====================================+
    | **Pros** | Tests whether results are robust | Reproduces the results exactly      |
    +----------+----------------------------------+-------------------------------------+
    | **Cons** | Contains hidden variables        | Does not ensure results are correct |
    +----------+----------------------------------+-------------------------------------+

As an example of what can go wrong, check out `this nice article <https://physicstoday.scitation.org/do/10.1063/PT.6.1.20180822a/full/>`_ on Physics Today about the "**war over supercooled water**" (2010-2017) - a long-standing and harsh controversy on the phase diagram of water as obtained from computer simulations.

.. image:: ./images/water_combined.png

**Morals of the story**:

- accurate workflow description matters more than reproducibility *per sè*

- sharing of data and workflow is crucial

My own reproducible research: a lesson hard-learned
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/zenodo.png

- Automated reproduction and validation of data/plots

- Workflow documentation via `Org-mode <https://org-mode.org>`_

- Stored as `Zenodo <https://zenodo.org/>`_ record

  - `https://doi.org/10.5281/zenodo.1478600 <https://doi.org/10.5281/zenodo.1478600>`_ [2019]

  - `https://doi.org/10.5281/zenodo.6108958 <https://doi.org/10.5281/zenodo.6108958>`_ [2021]

  - `https://doi.org/10.5281/zenodo.7296251 <https://doi.org/10.5281/zenodo.7296251>`_ [2022]

  - `https://doi.org/10.5281/zenodo.11443197 <https://doi.org/10.5281/zenodo.11443197>`_ [2025]

- Provide `Docker <https://www.docker.com/>`_ images for full reproducibility

- Develop my own `workflow and data manager <https://framagit.org/coslo/pantarei>`_

- Export the project document `as a webpage <https://leonardogalliano.frama.io/find_onset/>`_ using sphinx

**Moral of the story**: fully reproducible research is time consuming!
