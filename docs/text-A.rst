


Getting started
---------------

This first tutorial builds on three key tools

1. **terminal**

2. **text editor**

3. **version control software** (here, ``git``)

Nowadays, they often come integrated into a single piece of software, but in the following I will consider them as individual entities.

At the end of the tutorial, you should be able to write collaboratively a **research document** or **code documentation** with a colleague of yours using ``git``. The research document will be built using `this Sphinx template <https://framagit.org/coslo/template-docs>`_, which renders `reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html>`_ or `markdown <https://www.markdownguide.org/basic-syntax/>`_ documents as a nice web page.

Terminal
~~~~~~~~

We'll use the terminal quite a lot in these tutorials, so we'll see a few tricks to work with it efficiently.

It is a good idea to work on separate folders for each tutorial. Let's create a folder for this tutorial.

.. code:: bash

    mkdir -p tools/tutorial_1/work

Note the ``-p`` flag towill create all parent directories in one shot.

Let's go in the new directory with ``cd`` (change directory)

.. code:: bash

    cd tools/tutorial_1/work

The prompt (the left-most part of the command line) should tell you where you are in your filesystem. You can always find it out by typing

.. code:: bash

    pwd

::

    /home/coslo/teaching/tools/docs/tools/tutorial_1/work


Let's see how to get back to the directory where we stood at the beginning. There are a few ways to do that:

- ``cd ..`` multiple times

- ``cd ../../../``

If you started from our **home** folder, you could also type

- ``cd ~``

- ``cd`` (!)

.. code:: bash

    cd ../../..

We'll start working - for good now - in the directory we created. Typing again ``cd tools/tutorial_1/work`` will work but is tedious. We can use instead a few general purpose **tricks** that make the interaction with the terminal really much faster and smoother.

**Master these tricks and you'll unleash the power of the bash shell!**

.. tip::

    **Trick**: *the terminal remembers the commands you type*

There is a **history** of the commands you typed (stored in ``~/.bash_history)``. Just use the up/down arrows to navigate your history of commands and you will find the command again.

.. tip::

    **Magic trick**: *TAB is you best friend*

The ``tab`` key completes (almost) anything on the command line. Start typing the command you have in mind and press ``tab``: the terminal will complete the path as much as possible. Keeping pressing ``tab`` to get suggestions!

Always keep your little finger close to ``tab`` and get used to press it (it will never hurt!), to complete long paths or commands or whenever you want to get in touch with your new best friend.

.. tip::

    **Mind-blowing trick**: *type commands at the speed of thought with ctrl-r*

Interacting with the terminal can be done at the speed of thought if you learn to use the ``ctrl-r`` shortcut. The best thing is to try it out yourself:

- type ``ctrl-r`` then type the beginning of the command you entered before... boom!

- press enter to execute it immediately

- press ``tab`` or the ``arrows`` to edit the command

- you can keep typing ``ctrl-r`` until you hit the command you want.

- ``ctrl-r`` also matches text *within* commands

- ``Esc`` or ``ctrl-g`` will let you get out of the recursive search

.. tip::

    **Extra trick:** *move quickly on the command line with shortcuts*

Moving around the command line is greatly facilitated by using the following emacs-like shortcuts

- ``ctrl-a``: go to the beginning of the line

- ``ctrl-e``: go to the end of the line

- ``alt-left``: move left one word at a time

- ``alt-right``: move right one word at a time

- ``ctrl-k``: cut everything that appears on right of the cursor and store it in memory

- ``ctrl-y``: paste the content in memory at the position of the cursor

**Goodies**:

- Chain commands with ``&&`` and ``||``

- Select texts in a rectangle

  - Linux: **Ctrl and mouse select**

  - WSL: Alt and mouse select

  - MacOS: Option and mouse select

**Environment**:

- Get control of your environment with ``~/.bashrc``

- Check and customize your **prompt**

Editors
~~~~~~~

- **Goals**:

  1. note taking / todo lists

  2. writing research documents, scientific articles (cf. collaborative editors)

  3. code development (cf. IDEs)

  4. handle projects' workflow (cf. notebooks)

- **General purpose editors**: `emacs <https://emacsdocs.org/>`_ (since 1976), `vim <https://www.vim.org/>`_ (since 1976, through 1991), ...

- **Code editors** (**IDEs)**: `VS code <https://code.visualstudio.com/>`_ (since 2015), `atom <https://atom.io>`_ (2014-2022), `sublime <https://www.sublimetext.com/>`_ (since 2008), `Xcode <https://developer.apple.com/xcode/>`_ (since 2003), ...

- **Notebooks and literate programming**: `Jupyter <https://jupyter.org/>`_ (since 2015), `org-mode <https://orgmode.org/>`_ (since 2003), ...

- **Collaborative editing**: gxxgle docs, `etherpad <https://etherpad.org/>`_ (since 2008), `sharelatex <https://en.wikipedia.org/wiki/Overleaf#Merge_with_ShareLaTeX>`_ (2013-2017), `overleaf <https://www.overleaf.com/>`_ (since 2014) ...

Before we start...
~~~~~~~~~~~~~~~~~~

- What kind of computational science project are you working on?

- Have you used ``git`` before?

.. table::

    +------+---------+-----+
    | Name | Project | git |
    +======+=========+=====+
    | \    | \       | \   |
    +------+---------+-----+
