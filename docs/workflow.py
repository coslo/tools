import time
import numpy
import matplotlib.pyplot as plt

def run(seed=1, n=300):
    """Return some Gaussian distributed data"""
    time.sleep(5.0)  # pretend to do some long calculation
    numpy.random.seed(seed)
    return numpy.random.normal(size=n)

def postprocess(data):
    return {'mean': numpy.mean(data), 'std': numpy.std(data)}

def plot(data, results):
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots()
    mean, std = results['mean'], results['std']
    ax.hist(data, histtype='step')
    ax.set_title(f'mean={mean:.3f} std={std:.3f}')
    fig.savefig('images/gauss.svg')

from pprint import pprint
data = run()
results = postprocess(data)
pprint(results)
plot(data, results)

from joblib import Memory

memory = Memory('.joblib')
cached_run = memory.cache(run)
data = cached_run()
results = postprocess(data)
pprint(results)

from pantarei import Task

data = Task(run)()
results = postprocess(data)
plot(data, results)
pprint(results)
