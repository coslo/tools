======================================
Modern tools for computational science
======================================

This is an informal presentation of tools and ideas to manage `computational science <https://en.wikipedia.org/wiki/Computational_science>`_ projects. The material is meant to be discussed in class during the course "Modern tools for computational physics" for the `Ph.D. program <https://web.units.it/dottorato/fisica/>`_ in Physics at the University of Trieste.

There are three main tutorials, to deal with `text <https://coslo.frama.io/tools/text-A.html>`_, `code <https://coslo.frama.io/tools/code-A.html>`_ and `projects <https://coslo.frama.io/tools/project-A.html>`_. They are also available as

- org-mode files (`text <https://framagit.org/coslo/tools/-/blob/master/docs/text.org>`_, `code <https://framagit.org/coslo/tools/-/blob/master/docs/code.org>`_, `project <https://framagit.org/coslo/tools/-/blob/master/docs/project.org>`_)
- jupyter notebooks (`text <https://framagit.org/coslo/tools/-/blob/master/docs/text.ipynb>`_, `code <https://framagit.org/coslo/tools/-/blob/master/docs/code.ipynb>`_, `project <https://framagit.org/coslo/tools/-/blob/master/docs/project.ipynb>`_)

Here are some templates used in the tutorials:

- `Documentation web page <https://framagit.org/coslo/template-docs>`_
- `Python package <https://framagit.org/coslo/template-python>`_
- `Bare-bones computational project <https://framagit.org/coslo/template-project>`_

**Author**

`Daniele Coslovich <https://www.units.it/daniele.coslovich/>`_

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Text
   :glob:
      
   text-*

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Code
   :glob:

   code-*

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Projects
   :glob:

   project-*
