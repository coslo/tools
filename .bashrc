# Aliases

alias w="watch"
alias ssh="ssh -X"
alias shx="chmod u+x *.sh"
alias pyx="chmod u+x *.py"
alias grep="grep --color=auto"
alias ls="ls --color=always"
alias ll="ls -l --group-directories-first"
alias la="ls -a"
alias lt="ls -ltr"
alias cp="cp -i"
alias rm="rm -i"
alias mv="mv -i"



# User defined functions

gitall() {
    # Stage and commit all new and modified files in git repo
    # http://stackoverflow.com/questions/2419249/git-commit-all-files-using-single-command
    git add --all && git commit -m "$*"
}

# Paths and environmental variables
export EDITOR="emacs -nw"
export LC_COLLATE=POSIX
export PATH=$PATH:$HOME/bin:$HOME/.local/bin

# Ignore python warnings
export PYTHONWARNINGS="ignore"



# Bash history setup, see
# - http://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows
# - http://askubuntu.com/questions/80371/bash-history-handling-with-multiple-terminals

export HISTFILESIZE=1000000  # set max length of ~/.bash_history
export HISTCONTROL=ignoredups:erasedups  # avoid duplicates
shopt -s histappend  # append history entries
# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
# Recursive globbing
shopt -s globstar



# Enable programmable completion features (you don't need to enable this, if it's already enabled in /etc/bash.bashrc and /etc/profile sources /etc/bash.bashrc).

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi



# We use a custom prompt that deals with both git and python virtual envs. From https://stackoverflow.com/questions/23399183/bash-command-prompt-with-virtualenv-and-git-branch/

# If this is an xterm set the title to user@host:dir
case "$TERM" in
    xterm*|rxvt*)
        PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
	;;
    *)
	;;
esac

# The various escape codes that we can use to color our prompt.
        RED="\[\033[0;31m\]"
     YELLOW="\[\033[1;33m\]"
      GREEN="\[\033[0;32m\]"
       BLUE="\[\033[1;34m\]"
  LIGHT_RED="\[\033[1;31m\]"
LIGHT_GREEN="\[\033[1;32m\]"
      WHITE="\[\033[1;37m\]"
 LIGHT_GRAY="\[\033[0;37m\]"
       GRAY='\[\033[0;36m\]'
       NONE="\[\e[0m\]"

# Detect whether the current directory is a git repository.
function is_git_repository {
  git branch > /dev/null 2>&1
}

function parse_git_dirty() {
  [[ $(git status 2> /dev/null | tail -n1) != *"clean"* ]] && echo "*"
}

function parse_git_branch() {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \(.*\)/\1$(parse_git_dirty)/"
}

function set_git_branch {
  # Set the final branch string
  BRANCH="${RED}($(parse_git_branch))${NONE}"
}

# Return the prompt symbol to use, colorized based on the return value of the
# previous command.
function set_prompt_symbol () {
  if test $1 -eq 0 ; then
      PROMPT_SYMBOL="\$"
  else
      PROMPT_SYMBOL="${LIGHT_RED}\$${NONE}"
  fi
}

# Determine active Python virtualenv details.
function set_virtualenv () {
  if test -z "$VIRTUAL_ENV" ; then
      PYTHON_VIRTUALENV=""
  else
      base=$(basename $VIRTUAL_ENV)
      env=$(basename $(dirname $VIRTUAL_ENV))
      PYTHON_VIRTUALENV="${GREEN}[$env/$base]${NONE}"
  fi
}

# Set the full bash prompt.
function set_bash_prompt () {
  # Set the PROMPT_SYMBOL variable. We do this first so we don't lose the
  # return value of the last command.
  set_prompt_symbol $?

  # Set the PYTHON_VIRTUALENV variable.
  set_virtualenv

  # Set the BRANCH variable.
  if is_git_repository ; then
    set_git_branch
  else
    BRANCH=''
  fi

  # Set the bash prompt variable.
  PS1="\u@\h:\w${PYTHON_VIRTUALENV}${BRANCH}${PROMPT_SYMBOL} "
}

# Tell bash to execute this function just before displaying its prompt.
PROMPT_COMMAND=set_bash_prompt



# Finally load local configuration (machine dependent)

if [ -f ~/.bashrc.local ] ; then
    . ~/.bashrc.local
fi
